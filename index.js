module.exports = {

  Is: require('./src/Is'),
  IsArray: require('./src/IsArray'),
  IsBoolean: require('./src/IsBoolean'),
  IsDate: require('./src/IsDate'),
  IsNull: require('./src/IsNull'),
  IsNumber: require('./src/IsNumber'),
  IsObject: require('./src/IsObject'),
  IsRealObject: require('./src/IsRealObject'),
  IsString: require('./src/IsString'),
  IsSymbol: require('./src/IsSymbol'),
  IsUndefined: require('./src/IsUndefined')

}